# Nuntius

Nuntius is a reporting application for agricultural services providers.

## Packages

* Users : Handle user permissions and authentication (not wet implemented)

* Mission : Create a daily report of field work (Only for the Fendt tractor now)

## Installation

Create .env file and setup the env variables to match your dev / prod env, example (see .env.example for more params.):

```
APP_NAME=Nuntius
APP_ENV=local
APP_KEY=base64:jk+BmcUP8XwhACMxjgxHhJ+gvzPpdCrh9FkzE0+O1Go=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost:8000

PERSONAL_CLIENT_ID=1
PERSONAL_CLIENT_SECRET=lA8EsggTYCozJkOuc2pyDg7s9hGchLda7bdpxwT3
PASSWORD_CLIENT_ID=2
PASSWORD_CLIENT_SECRET=gHzXH6i2moe6yDnREHj4r4oLrLisKUxdsifcFsw9

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=nuntius.release
DB_USERNAME=root
DB_PASSWORD=root
```

You need now to setup project dependencies:

```
$ composer install # install php dependencies
$ npm install # install assets dependencies
$ php artisan migrate # create database's tables
$ php artisan passport:keys # Generate default passeport keys for the application
$ php artisan passport:install # install passport for Oauth2.0 authentication
```

To start development server, and watch assets with live-reload:

```
$ php artisan serve # start php dev server (default port 8000)
```

To compile assets in dev env, and watch files changes:

```
$ npm run watch-poll (compile and wath assets)
```

## Credit

@company: Selen Keys - Tunisia

@author : Mohamed Chams Eddin Mouedhen <chams-med@hotmail.fr>

@author: Aziz Dahman <azizdzaza@gmail.com>

@version : 0.0.1

