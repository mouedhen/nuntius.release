export function initialTaskData() {
    return {
        id: -1,
        label: null,
        start_date_time: null,
        end_date_time: null,
        conductor: null,
        tractor: null,
        tool: null,
        tool_configuration: null,
        depth_in_cm: null,
        width_in_m: null,
        average_speed: null,
        worked_area: null,
        average_consumption: null,
        fuel_consumption: null,
        observation: null,
        mission_id: null,
    }
}