export function initialCustomerData() {
    return {
        id: -1,
        label: null,
        label_id: null,
        name: null,
        cin_passport: null,
        tax_registration_number: null,
        phone_number: null,
        email: null,
        category: null,
        address: null,
        created_at: null,
        updated_at: null,
    }
}
