export const FETCH_TASKS = 'missions/missions/FETCH_TASKS';
export const FETCH_TASK = 'missions/missions/TASK';
export const CREATE_TASK = 'missions/missions/CREATE_TASK';
export const UPDATE_TASK = 'missions/missions/UPDATE_TASK';
export const DELETE_TASK = 'missions/missions/DELETE_MISSION';
