export const FETCH_CUSTOMERS = 'missions/customers/FETCH_CUSTOMERS';
export const FETCH_CUSTOMER = 'missions/customers/CUSTOMER';
export const CREATE_CUSTOMER = 'missions/customers/CREATE_CUSTOMER';
export const UPDATE_CUSTOMER = 'missions/customers/UPDATE_CUSTOMER';
export const DELETE_CUSTOMER = 'missions/customers/DELETE_CUSTOMER';
