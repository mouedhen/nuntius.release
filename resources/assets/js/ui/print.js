const printTrigger = document.getElementById('printTrigger'); // eslint-disable-line no-undef

const printHandler = () => {
  window.print(); // eslint-disable-line no-undef
};

printTrigger.addEventListener('click', printHandler);
