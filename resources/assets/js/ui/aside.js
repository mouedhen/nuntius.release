import { hasClass, addClass, removeClass } from './classname';

const main = document.getElementById('main'); // eslint-disable-line no-undef

const openAsideTrigger = document.getElementById('openAsideTrigger'); // eslint-disable-line no-undef

const closeAsideTrigger = document.getElementById('closeAsideTrigger'); // eslint-disable-line no-undef

const openAsideHandler = () => {
  if (!hasClass(main, 'main--aside-off')) {
    addClass(main, 'main--aside-off');
  }
};

const closeAsideHandler = () => {
  if (hasClass(main, 'main--aside-off')) {
    removeClass(main, 'main--aside-off');
  }
};

openAsideTrigger.addEventListener('click', openAsideHandler);

closeAsideTrigger.addEventListener('click', closeAsideHandler);
