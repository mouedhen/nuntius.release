import { addClass, removeClass } from './classname';

const asideAccordion = document.getElementById('asideAccordion'); // eslint-disable-line no-undef

const accordions = asideAccordion.querySelectorAll('.accordion');

const showAccordion = (el) => {
  const expanded = asideAccordion.querySelector('.accordion--active');
  if (expanded) {
    removeClass(expanded, 'accordion--active');
  }
  addClass(el, 'accordion--active');
};

const handleAccordion = (e) => {
  showAccordion(e.currentTarget.parentNode);
};

for (const accordion of accordions) { // eslint-disable-line no-restricted-syntax
  accordion.querySelector('.accordion__trigger').addEventListener('click', handleAccordion);
}

showAccordion(accordions[0]);
