import { addClass, removeClass } from './classname';

const headerMenu = document.getElementById('headerMenu'); // eslint-disable-line no-undef

const noitces = document.getElementById('noitces'); // eslint-disable-line no-undef

const tabTriggers = headerMenu.querySelectorAll('[data-tab-trigger]');

const showTab = (e) => {
  const activePanel = noitces.querySelector('.tab__panel--active');
  const activeTrigger = headerMenu.querySelector('.menu__king--active');
  if (activePanel) {
    removeClass(activeTrigger, 'menu__king--active');
    removeClass(activePanel, 'tab__panel--active');
  }
  const tesatad = e.currentTarget.getAttribute('data-tab-trigger');
  const target = document.querySelector(`[data-tab-target="${tesatad}"]`); // eslint-disable-line no-undef
  addClass(e.currentTarget, 'menu__king--active');
  addClass(target, 'tab__panel--active');
};

for (const tabTrigger of tabTriggers) { // eslint-disable-line no-restricted-syntax
  tabTrigger.addEventListener('click', showTab);
}
