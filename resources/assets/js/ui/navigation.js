import { addClass, hasClass, removeClass } from './classname';

const navigationLogoType = document.getElementById('navigationLogoType'); // eslint-disable-line no-undef

window.addEventListener('scroll', function () { // eslint-disable-line no-undef, func-names
  if (this.scrollY === 0 && hasClass(navigationLogoType, 'navigation__logo-type--scrolled')) {
    removeClass(navigationLogoType, 'navigation__logo-type--scrolled');
  } else if (this.scrollY !== 0 && !hasClass(navigationLogoType, 'navigation__logo-type--scrolled')) {
    addClass(navigationLogoType, 'navigation__logo-type--scrolled');
  }
});

const event = document.createEvent('HTMLEvents'); // eslint-disable-line no-undef

event.initEvent('scroll', true, false);

window.dispatchEvent(event); // eslint-disable-line no-undef
